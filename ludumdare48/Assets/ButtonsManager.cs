﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsManager : MonoBehaviour
{
    public void GoToMenu()
    {
        SceneChanger.Get().GoToMenu();
    }

    public void GoToGame()
    {
        SceneChanger.Get().GoToGame();
    }

    public void GoToEndScene()
    {
        SceneChanger.Get().GoToEndScene();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
