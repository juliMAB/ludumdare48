﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{ 
    private float charge;
    private CHARGE ChargeState;
    [SerializeField] private bool unlookEndDoor;
    [SerializeField] private int Cant_notas=0;
    [SerializeField] private PlayerManager pj;
    [SerializeField] private TMPro.TMP_Text notes;
    [SerializeField] private TMPro.TMP_Text intro;
    [SerializeField] public GameObject BatteryFull;
    [SerializeField] public GameObject BatteryTwoThirds;
    [SerializeField] public GameObject BatteryOneThirds;
    [SerializeField] public GameObject BatteryEmpty;
    [SerializeField] public bool scenebegin = false;
    [SerializeField] public bool sceneover = false;
    [SerializeField] public bool playsound = false;
    [SerializeField] public float timer=7;

    public GameObject puerta;

    public bool win = false;
    public bool lose = false;


    private enum CHARGE
    {
        FULL, TWOTHIRD, ONETHIRD, EMPTY
    };


    void Start()
    {
        unlookEndDoor = false;
    }

    // Update is called once per frame
    void Update()
    {
        charge = pj.charge;
        updateNotas();
        updateIntro();
        ShowBatteryUI();

        if (puerta.GetComponent<doorManager>().Dentro() && !lose)
            win = true;

        if (pj.vida <= 0 && !win&&!scenebegin)
        {
            scenebegin = true;
            playsound = true;
        }

        if (playsound)
        {
            AkSoundEngine.PostEvent("Muerte_Final", pj.gameObject);
            playsound = false;
        }

        if (scenebegin)
        {
            timer -= Time.deltaTime;
            pj.transform.LookAt(Vector3.forward);
            if (timer <= 0)
            {
                sceneover = true;
            }
        }
        if (sceneover)
        {
            lose = true;
        }

        if (win || lose)
        {
            SceneChanger.Get().win = win;
            SceneChanger.Get().lose = lose;
        }
    }
    void updateNotas()
    {
        if (pj.PickUpNote())
        {
            Cant_notas++;
            notes.text = "" + Cant_notas;
        }

        if (Cant_notas == 8 && puerta.GetComponent<doorManager>().loked)
            puerta.GetComponent<doorManager>().loked = false;
    }
    void updateIntro()
    {
        if (intro.alpha <= 0)
        {
            intro.alpha = 0;
        }
        else
        {
            intro.CrossFadeAlpha(0, 2, false);
        }

    }
    void ShowBatteryUI()
    {
        if (charge > 75)
        {
            ChargeState = CHARGE.FULL;
        }
        else if (charge > 50)
        {
            ChargeState = CHARGE.TWOTHIRD;
        }
        else if (charge > 25)
        {
            ChargeState = CHARGE.ONETHIRD;
        }
        else if (charge > 0)
        {
            ChargeState = CHARGE.EMPTY;
        }
        switch (ChargeState)
        {
            case CHARGE.FULL:
                BatteryFull.SetActive(true);
                break;
            case CHARGE.TWOTHIRD:
                BatteryFull.SetActive(false);
                BatteryTwoThirds.SetActive(true);
                break;
            case CHARGE.ONETHIRD:
                BatteryTwoThirds.SetActive(false);
                BatteryOneThirds.SetActive(true);
                break;
            case CHARGE.EMPTY:
                BatteryOneThirds.SetActive(false);
                BatteryEmpty.SetActive(true);
                break;
        }
    }
}
