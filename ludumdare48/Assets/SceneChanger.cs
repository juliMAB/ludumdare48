﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    private static SceneChanger instance;

    public bool win;
    public bool lose;

    private bool inEndScene = false;

    /*public struct FinalData
    {
        public int score;
        public int life;
    }

    FinalData fData;

    FinalData SetFinalData()
    {
        /*PlayerManager pm = GameObject.FindWithTag("Player").GetComponent<PlayerManager>();
        ShotsManager sm = GameObject.FindWithTag("Player").GetComponent<ShotsManager>();

        FinalData fd;

        fd.bullets = sm.totalBullets;
        fd.chargers = sm.totalChargers;
        fd.life = pm.GetLife();
        fd.score = pm.GetScore();

        return fd;
    //}

    public FinalData GetFinalData()
    {
        return fData;
    }*/

    public class FinalUserData
    {
        public int score;
    }

    public static FinalUserData finalData;

    public static SceneChanger Get()
    {
        return instance;
    }

    public static string GetSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    void ResetEndConditions()
    {
        if (win)
            win = false;

        if (lose)
            lose = false;

        if (inEndScene)
            inEndScene = false;
    }

    public void GoToGame()
    {
        ResetEndConditions();
        SceneManager.LoadScene("JuliScene2");
    }

    public void GoToEndScene()
    {
        ResetEndConditions();
        SceneManager.LoadScene("EndScene");
    }

    public void GoToMenu()
    {
        ResetEndConditions();
        SceneManager.LoadScene("MainMenu 1");
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log(name + " has been destroyed, there's already one Scene Manager created.");
            return;
        }
        instance = this;
        Debug.Log(name + " has been created and is now the Scene Manager.");
        DontDestroyOnLoad(gameObject);
        win = false;
        lose = false;
    }

    private void Update()
    {
        if ((win || lose) && !inEndScene)
        {
            //fData = SetFinalData();
            GoToEndScene();
            inEndScene = true;  
        }
    }
}
