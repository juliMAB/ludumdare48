﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndSceneOverlayManager : MonoBehaviour
{
    [SerializeField] Text resultText;

    void Start()
    {
        if (SceneChanger.Get().win)
            resultText.text = "You've escaped";
        else
            resultText.text = "You're dead";
    }
}
