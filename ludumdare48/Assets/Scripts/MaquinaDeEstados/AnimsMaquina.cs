﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimsMaquina : MonoBehaviour
{
    public MaquinaDeEstados ME;
    public Animator animator;
    public pegaralplayer pegaralplayer;

    private void Update()
    {
        switch (ME.estadoActual)
        {
            case EstadoAlerta estadoAlerta:
                MakeAllFalse();
                animator.SetBool("alert",true);
                break;
            case EstadoPatrulla estadoPatrulla:
                MakeAllFalse();
                animator.SetBool("walking", true);
                break;
            case EstadoPersiguiendo estadoPersiguiendo:
                MakeAllFalse();
                if (pegaralplayer.LoTieneAdentro)
                {
                    animator.SetBool("ataking", true);
                    pegaralplayer.LoTieneAdentro = false;
                }
                else
                {
                    animator.SetBool("runing", true);
                }
               
                break;
        }

    }

    void MakeAllFalse()
    {
        animator.SetBool("iddle", false);
        animator.SetBool("walking", false);
        animator.SetBool("runing", false);
        animator.SetBool("alert", false);
        animator.SetBool("ataking", false);
    }
    
}
