﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFrostum : MonoBehaviour
{
    [SerializeField] public Camera camera;

    void Start()
    {
        
    }

    void CheckObjectsInScene()
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        if (GeometryUtility.TestPlanesAABB(planes, this.GetComponent<Collider>().bounds))
        {
            //print("The object" + obj.name + "has appeared");
            this.GetComponent<MeshRenderer>().enabled = true;
        }
        else
        {
            //print("The object" + obj.name + "has disappeared");
            this.GetComponent<MeshRenderer>().enabled = false;
        }


    }

    void Update()
    {
        CheckObjectsInScene();
    }
}
