﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ControladorNavMesh : MonoBehaviour
{
    public Transform objetivo;
    public Transform player;
    [SerializeField] public NavMeshAgent navMeshAgent;
    [SerializeField] public float trololo;

    private void Awake()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void ActualizarPuntoDeDestino(Vector3 puntoDestino)
    {
        navMeshAgent.destination = puntoDestino;
    
        if (navMeshAgent.isStopped)
            navMeshAgent.isStopped = false;
    }
    
    public void ActualizarPuntoDeDestino()
    {
        ActualizarPuntoDeDestino(objetivo.position);
    }
    
    public void Detener()
    {
        navMeshAgent.isStopped = true;
    }
    
    public bool LlegoADestino()
    {
        return navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending;
    }
}
