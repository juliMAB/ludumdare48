﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorView : MonoBehaviour
{
    public float tiempoDeCoolDownTotal = 10f;
    public float timeCoolDown = 10f;
    public bool viendoAPlayer = false;

    public GameObject visiblePlayer;
    public MeshRenderer ViendoAlPlayerIndicador;
    //public GameObject 

    public Transform ojos;
    public float rangoVision = 90f;
    
    public Vector3 offSet = new Vector3(0f, 0.75f, 0f);
    
    private ControladorNavMesh controladorNavMesh;
    private MaquinaDeEstados maquinaDeEstados;

    

    private void Awake()
    {
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        maquinaDeEstados = GetComponent<MaquinaDeEstados>();
    }

    public static bool InRoom(Vector3 position, Plane[] roomPlanes, int tam)
    {
        for (int i = 0; i < tam; i++)
        {
            for (int j = 0; j < tam - 1; j++)
            {
                if (roomPlanes[i].GetSide(position) != roomPlanes[j].GetSide(position))
                    return false;
            }
        }

        return true;
    }

    public static bool CheckOneVertexInRoom(GameObject go, Plane[] roomPlanes, int tam)
    {
        Mesh mesh = go.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            if (InRoom(go.transform.TransformPoint(vertices[i]), roomPlanes, tam))
                return true;
        }

        return false;
    }

    /*public bool ViendoAlJugador(out RaycastHit hit, bool mirandoAJugador = false)//
    {
        // Plane[] planes = GeometryUtility.CalculateFrustumPlanes(maquinaDeEstados.cam);
        //return ObjectInRoom.CheckOneVertexInRoom(visiblePlayer, planes, planes.Length);

        Vector3 vecDir;

        if (mirandoAJugador)
        {
            vecDir = (controladorNavMesh.objetivo.position + offSet) - ojos.position;
        }
        else
        {
            vecDir = ojos.forward;
        }

        return Physics.Raycast(ojos.position, vecDir, out hit, rangoVision) && hit.collider.tag == "Player";

        

        
    }*/

    int rayCount = 100;
    float angle = -60f;
    float angleIncrease = 120f / 100f; //el 100f es raycount, pero no me deja ponerlo asi que messi
    float viewDistance = 500f;

    Vector3 GetVectorFromAngle(float angle)
    {
        float angleRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(angleRad), 0f, Mathf.Sin(angleRad));
    }

    float GetAngleFromVector(Vector3 dir)
    {
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        if (n < 0) n += 360;

        return n;
    }

    public bool ViendoAlJugador(out RaycastHit hit)//
    {
        Vector3 vecDir;

        for (int i = 0; i < rayCount; i++)
        {
            vecDir = GetVectorFromAngle(angle).normalized;

            //if (vecDir.z > 0 && vecDir.x <= 0.5 && vecDir.x >= -0.5 && vecDir.y > -0.5)
            //{
                if (Physics.Raycast(ojos.position, vecDir, out hit, viewDistance) && hit.collider.tag == "Player")
                {
                    if (!viendoAPlayer)
                    { 
                        viendoAPlayer = true;
                    }
                    timeCoolDown = tiempoDeCoolDownTotal;
                    angle = 0f;
                    return Physics.Raycast(ojos.position, vecDir, out hit, viewDistance) && hit.collider.tag == "Player";
                }
            //}

            angle += angleIncrease;
        }

        vecDir = ojos.forward;

        angle = 0f;

        return Physics.Raycast(ojos.position, vecDir, out hit, viewDistance) && hit.collider.tag == "Player";
    }

    void Update()
    {
        Debug.DrawRay(ojos.position, ojos.forward, Color.red);

        RaycastHit hit;

        if (viendoAPlayer && !ViendoAlJugador(out hit))
            timeCoolDown -= Time.deltaTime;
        else
            timeCoolDown = tiempoDeCoolDownTotal;

        if (timeCoolDown <= 0f)
            viendoAPlayer = false;

        if (ViendoAlJugador(out hit))
            Debug.DrawLine(ojos.position, hit.collider.transform.position, Color.red, 1);
        //else
            //Debug.DrawLine(ojos.position, hit.collider.transform.position, Color.green, 1);*/

        if (!ViendoAlJugador(out hit) && !viendoAPlayer)
        {
            ViendoAlPlayerIndicador.material.color = Color.green;
        }
        else
        {
            ViendoAlPlayerIndicador.material.color = Color.red;
        }
    }
}
