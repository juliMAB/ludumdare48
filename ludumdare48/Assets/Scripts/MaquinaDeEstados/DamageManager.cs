﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageManager : MonoBehaviour
{
    bool damageTimerSet = false;
    float timeToNextHit = 1f;

    private MaquinaDeEstados maquinaDeEstados;

    void Awake()
    {
        maquinaDeEstados = GetComponent<MaquinaDeEstados>();

        if (DamageUnable()) GetComponent<Collider>().enabled = false;
    }

    bool DamageUnable()
    {
        return (MaquinaDeEstados.Mood)(maquinaDeEstados.actualMood) == MaquinaDeEstados.Mood.neutral;
    }

    private void Update()
    {
        if (damageTimerSet)
        {
            timeToNextHit -= Time.deltaTime;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!damageTimerSet)
        {   
            damageTimerSet = true;
            timeToNextHit = 0f;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (timeToNextHit <= 0f)
        {
            //sacarle vida al player

            timeToNextHit = 1f;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (damageTimerSet)
        {
            damageTimerSet = false;
        }
    }
}
