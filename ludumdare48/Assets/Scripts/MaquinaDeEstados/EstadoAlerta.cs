﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoAlerta : MonoBehaviour
{
    public float velocidadGiroBusqueda = 120f;
    public float duracionBusqueda = 4f;

    private float tiempoBuscando;

    public Color colorEstado = Color.yellow;

    private MaquinaDeEstados maquinaDeEstados;
    private ControladorNavMesh controladorNavMesh;
    private ControladorView controladorView;

    void Awake()
    {
        maquinaDeEstados = GetComponent<MaquinaDeEstados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorView = GetComponent<ControladorView>();
        //Debug.Log("pos: " + transform.position.ToString());
    }

    void Update()
    {
        RaycastHit hit;
        if (controladorView.ViendoAlJugador(out hit))//out hit
        {
            controladorNavMesh.objetivo = hit.transform; 
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.persecucion);
            return;
        }

        transform.Rotate(0f, velocidadGiroBusqueda * Time.deltaTime, 0f);
        tiempoBuscando += Time.deltaTime;
        if(tiempoBuscando>=duracionBusqueda)
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.patrulla);
            return;
        }
    }

    private void OnEnable()
    {
        maquinaDeEstados.indicadorMesh.material.color = colorEstado;
        controladorNavMesh.Detener();
        tiempoBuscando = 0f;
    }
}
