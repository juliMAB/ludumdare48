﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EstadoPatrulla : MonoBehaviour
{
    public Transform[] wayPoints;
    public Color colorEstado = Color.green;

    private MaquinaDeEstados maquinaDeEstados;
    private ControladorNavMesh controladorNavMesh;
    private ControladorView controladorView;
    public int siguienteWayPoint;

    void Awake()
    {
        maquinaDeEstados = GetComponent<MaquinaDeEstados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorView = GetComponent<ControladorView>();
        //Debug.Log("pos: " + transform.position.ToString());
    }

    void ActualizarDestino()
    {
        controladorNavMesh.ActualizarPuntoDeDestino(wayPoints[siguienteWayPoint].position);
    }

    void Update()
    {
        RaycastHit hit;
        
        if (controladorView.ViendoAlJugador(out hit))//out hit
        {
            controladorNavMesh.objetivo = hit.transform; 
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.persecucion);
            return;
        }

        if (controladorNavMesh.LlegoADestino())
        {
             siguienteWayPoint = (siguienteWayPoint + 1) % wayPoints.Length;
             ActualizarDestino();
        }
    }
    
    void OnEnable()
    {
        maquinaDeEstados.indicadorMesh.material.color = colorEstado;
        ActualizarDestino();
    }

    public void OnTriggerEnter(Collider other)
    {
        if ((MaquinaDeEstados.Mood)(maquinaDeEstados.actualMood) == MaquinaDeEstados.Mood.neutral)
        {
            //lucas
            return;
        } 

        if (other.gameObject.CompareTag("Player") && enabled)
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.alerta);
        }
    }
}

/*
 
 
            switch ((MaquinaDeEstados.Mood)(maquinaDeEstados.actualMood))
            {
                case MaquinaDeEstados.Mood.neutral:
                    break;
                case MaquinaDeEstados.Mood.noNeutral:
                    break;
                case MaquinaDeEstados.Mood.agresivo:
                    break;
                case MaquinaDeEstados.Mood.last:
                    break;
                default:
                    break;
            }
 */

