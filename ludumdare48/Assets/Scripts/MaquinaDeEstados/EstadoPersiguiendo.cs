﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EstadoPersiguiendo : MonoBehaviour
{
    public Color colorEstado = Color.red;

    private MaquinaDeEstados maquinaDeEstados;
    private ControladorNavMesh controladorNavMesh;
    private ControladorView controladorView;

    void Awake()
    {
        maquinaDeEstados = GetComponent<MaquinaDeEstados>();
        controladorNavMesh = GetComponent<ControladorNavMesh>();
        controladorView = GetComponent<ControladorView>();
        //Debug.Log("pos: " + transform.position.ToString());
    }

    void Start()
    {
        
    }

    void Update()
    {
        RaycastHit hit;
        if(!controladorView.ViendoAlJugador(out hit) && !controladorView.viendoAPlayer)//, true
        {
            maquinaDeEstados.ActivarEstado(maquinaDeEstados.alerta);
            return;
        }

        controladorNavMesh.ActualizarPuntoDeDestino();
    }

    private void OnEnable()
    {
        maquinaDeEstados.indicadorMesh.material.color = colorEstado;
    }
}
