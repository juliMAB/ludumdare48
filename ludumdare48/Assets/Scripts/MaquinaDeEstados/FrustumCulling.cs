﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using UnityEngine.ProBuilder.MeshOperations;

public class FrustumCulling : MonoBehaviour
{
    public Transform[] t;

    public float maxDistance = 10f;
    public float minDistance = 0f;
    public float width = 12f;
    public float height = 8f;
    public float closeDiffInXFromCenter = 0.2f;
    public float closeDiffInYFromCenter = 0.1f;
    public frustrumCulling fc;

    public class frustrumCulling
    {
        public Plane[] planes = new Plane[6];
        public Vector3[] points = new Vector3[8];
        public Vector3 pointToPiramid = new Vector3();
        public Vector3[] rotation = new Vector3[8]; 
        public float maxDistance = 10f;
        public float minDistance = 0f;
        public float width = 12f;
        public float height = 8f;
        public float closeDiffInXFromCenter = 0.2f;
        public float closeDiffInYFromCenter = 0.1f;

        Vector3 GetVectorFromAngle(float angle)
        {
            float angleRad = angle * (Mathf.PI / 180f);
            return new Vector3(Mathf.Cos(angleRad), 0f, Mathf.Sin(angleRad));
        }

        Quaternion GetRotation(Vector3 angle)
        {
            Quaternion qx = Quaternion.identity;
            Quaternion qy = Quaternion.identity;
            Quaternion qz = Quaternion.identity;

            float sinAngleZ = Mathf.Sin(Mathf.Deg2Rad * angle.z * 0.5f);
            float cosAngleZ = Mathf.Cos(Mathf.Deg2Rad * angle.z * 0.5f);
            qz.Set(0, 0, sinAngleZ, cosAngleZ);

            float sinAngleX = Mathf.Sin(Mathf.Deg2Rad * angle.x * 0.5f);
            float cosAngleX = Mathf.Cos(Mathf.Deg2Rad * angle.x * 0.5f);
            qx.Set(sinAngleX, 0, 0, cosAngleX);

            float sinAngleY = Mathf.Sin(Mathf.Deg2Rad * angle.y * 0.5f);
            float cosAngleY = Mathf.Cos(Mathf.Deg2Rad * angle.y * 0.5f);
            qy.Set(0, sinAngleY, 0, cosAngleY);

            return qy * qx * qz;
        }

        public void CreatePoints(Transform tf)//, Transform[] t)
        {
            Vector3 pos = new Vector3(tf.position.x * Mathf.Cos(Mathf.Deg2Rad * tf.rotation.eulerAngles.x),
                                      tf.position.y,
                                      tf.position.x * Mathf.Cos(Mathf.Deg2Rad * tf.rotation.eulerAngles.x));
            Quaternion q = GetRotation(tf.rotation.eulerAngles);

            Vector3 rot = new Vector3(Mathf.Cos(Mathf.Deg2Rad * tf.rotation.eulerAngles.x), 0f,
                                      Mathf.Sin(Mathf.Deg2Rad * tf.rotation.eulerAngles.z));

            //t[0].position = new Vector3(-closeDiffInXFromCenter, -closeDiffInYFromCenter, minDistance);
            //t[0].rotation = GetRotation(tf.rotation.eulerAngles);

            points[0] = pos + new Vector3(-closeDiffInXFromCenter * rot.x, -closeDiffInYFromCenter, minDistance * rot.z);
            points[1] = pos + new Vector3(-closeDiffInXFromCenter * rot.x, closeDiffInYFromCenter, minDistance * rot.z);
            points[2] = pos + new Vector3(closeDiffInXFromCenter * rot.x, closeDiffInYFromCenter, minDistance * rot.z);
            points[3] = pos + new Vector3(closeDiffInXFromCenter * rot.x, -closeDiffInYFromCenter, minDistance * rot.z);
            points[4] = pos + new Vector3(-width / 2f * rot.x, -height / 2f, maxDistance * rot.z);
            points[5] = pos + new Vector3(-width / 2f * rot.x, height / 2f, maxDistance * rot.z);
            points[6] = pos + new Vector3(width / 2f * rot.x, height / 2f, maxDistance * rot.z);
            points[7] = pos + new Vector3(width / 2f * rot.x, -height / 2f, maxDistance * rot.z);

            /*
             
            points[0] = pos + new Vector3(-closeDiffInXFromCenter, -closeDiffInYFromCenter, minDistance);
            points[1] = pos + new Vector3(-closeDiffInXFromCenter, closeDiffInYFromCenter, minDistance);
            points[2] = pos + new Vector3(closeDiffInXFromCenter, closeDiffInYFromCenter, minDistance);
            points[3] = pos + new Vector3(closeDiffInXFromCenter, -closeDiffInYFromCenter, minDistance);
            points[4] = pos + new Vector3(-width / 2f, -height / 2f, maxDistance);
            points[5] = pos + new Vector3(-width / 2f, height / 2f, maxDistance);
            points[6] = pos + new Vector3(width / 2f, height / 2f, maxDistance);
            points[7] = pos + new Vector3(width / 2f, -height / 2f, maxDistance);

             */


            for (int i = 0; i < 8; i++)
            {
               points[i] = rotatepoints(points[i],tf);
            }
        }



        private Vector3 rotatepoints(Vector3 a, Transform eso)
        {
            Vector3 v3 = a;
            float dist = Vector3.Distance(a, eso.position);
            float angle = eso.transform.eulerAngles.y;
            v3.x += 1 * Mathf.Cos(angle);
            v3.z += 1 * Mathf.Sin(angle);
            a = v3;
            return a;
        }

        public void CreatePlanes()
        {
            Plane pToGetPlaneNormal = new Plane(points[2], points[1], points[0]);
            planes[0] = new Plane(pToGetPlaneNormal.normal, points[1]);

            pToGetPlaneNormal = new Plane(pointToPiramid, points[5], points[4]);
            planes[1] = new Plane(pToGetPlaneNormal.normal, points[1]);

            pToGetPlaneNormal = new Plane(pointToPiramid, points[6], points[5]);
            planes[2] = new Plane(pToGetPlaneNormal.normal, points[1]);

            pToGetPlaneNormal = new Plane(pointToPiramid, points[7], points[6]);
            planes[3] = new Plane(pToGetPlaneNormal.normal, points[3]);

            pToGetPlaneNormal = new Plane(points[7], pointToPiramid, points[4]);
            planes[4] = new Plane(pToGetPlaneNormal.normal, points[7]);

            pToGetPlaneNormal = new Plane(points[6], points[4], points[5]);
            planes[5] = new Plane(pToGetPlaneNormal.normal, points[7]);
        }
    }

    void Start()
    {
        fc = new frustrumCulling();

        fc.maxDistance = maxDistance;
        fc.minDistance = minDistance;
        fc.width = width;
        fc.height = height;
        fc.closeDiffInXFromCenter = closeDiffInXFromCenter;
        fc.closeDiffInYFromCenter = closeDiffInYFromCenter;

       updatePuntosYPlanos();
    }

    void updatePuntosYPlanos()
    {
        fc.CreatePoints(transform);
        fc.CreatePlanes();
    }
    void Update()
    {
        //transform.position = enemy.position;

        updatePuntosYPlanos();
        Debug.DrawLine(fc.points[1], fc.points[5], Color.red);
        Debug.DrawLine(fc.points[0], fc.points[4], Color.red);
        Debug.DrawLine(fc.points[4], fc.points[5], Color.red);
        Debug.DrawLine(fc.points[2], fc.points[6], Color.red);
        Debug.DrawLine(fc.points[5], fc.points[6], Color.red);
        Debug.DrawLine(fc.points[3], fc.points[7], Color.red);
        Debug.DrawLine(fc.points[6], fc.points[7], Color.red);
        Debug.DrawLine(fc.points[4], fc.points[7], Color.red);
        Debug.DrawLine(fc.points[0], fc.points[1], Color.red);
        Debug.DrawLine(fc.points[1], fc.points[2], Color.red);
        Debug.DrawLine(fc.points[2], fc.points[3], Color.red);
        Debug.DrawLine(fc.points[3], fc.points[0], Color.red);

        /*//normal 1
        Debug.DrawLine(transform.localPosition, transform.localPosition + fc.planes[0].normal, Color.green);
        Debug.DrawLine(transform.localPosition, transform.localPosition + fc.planes[0].normal / 2f, Color.blue);

        //normal 2
        Debug.DrawLine(transform.localPosition + new Vector3(-fc.width/2f, 0f, fc.maxDistance / 2f), transform.localPosition + new Vector3(-fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[1].normal , Color.green);
        Debug.DrawLine(transform.localPosition + new Vector3(-fc.width / 2f, 0f, fc.maxDistance / 2f), transform.localPosition + new Vector3(-fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[1].normal /2f, Color.blue);

        //normal 3
        Debug.DrawLine(transform.localPosition + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f), transform.localPosition + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f) + fc.planes[2].normal, Color.green);
        Debug.DrawLine(transform.localPosition + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f), transform.localPosition + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f) + fc.planes[2].normal / 2f, Color.blue);

        //normal 4
        Debug.DrawLine(transform.localPosition + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f), transform.localPosition + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[3].normal, Color.green);
        Debug.DrawLine(transform.localPosition + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f), transform.localPosition + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[3].normal / 2f, Color.blue);

        //normal 5
        Debug.DrawLine(transform.localPosition + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f), transform.localPosition + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f) + fc.planes[4].normal, Color.green);
        Debug.DrawLine(transform.localPosition + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f), transform.localPosition + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f) + fc.planes[4].normal / 2f, Color.blue);

        //normal 6
        Debug.DrawLine(transform.localPosition + new Vector3(0f, 0f, fc.maxDistance), transform.localPosition + new Vector3(0f, 0f, fc.maxDistance) + fc.planes[5].normal, Color.green);
        Debug.DrawLine(transform.localPosition + new Vector3(0f, 0f, fc.maxDistance), transform.localPosition + new Vector3(0f, 0f, fc.maxDistance) + fc.planes[5].normal / 2f, Color.blue);*/


        //normal 1
        Debug.DrawLine(transform.position, transform.position + fc.planes[0].normal, Color.green);
        Debug.DrawLine(transform.position, transform.position + fc.planes[0].normal / 2f, Color.blue);

        //normal 2
        Debug.DrawLine(transform.position + new Vector3(-fc.width/2f, 0f, fc.maxDistance / 2f), transform.position + new Vector3(-fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[1].normal , Color.green);
        Debug.DrawLine(transform.position + new Vector3(-fc.width / 2f, 0f, fc.maxDistance / 2f), transform.position + new Vector3(-fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[1].normal /2f, Color.blue);

        //normal 3
        Debug.DrawLine(transform.position + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f), transform.position + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f) + fc.planes[2].normal, Color.green);
        Debug.DrawLine(transform.position + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f), transform.position + new Vector3(0f, fc.height / 2f, fc.maxDistance / 2f) + fc.planes[2].normal / 2f, Color.blue);

        //normal 4
        Debug.DrawLine(transform.position + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f), transform.position + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[3].normal, Color.green);
        Debug.DrawLine(transform.position + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f), transform.position + new Vector3(fc.width / 2f, 0f, fc.maxDistance / 2f) + fc.planes[3].normal / 2f, Color.blue);

        //normal 5
        Debug.DrawLine(transform.position + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f), transform.position + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f) + fc.planes[4].normal, Color.green);
        Debug.DrawLine(transform.position + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f), transform.position + new Vector3(0f, -fc.height / 2f, fc.maxDistance / 2f) + fc.planes[4].normal / 2f, Color.blue);

        //normal 6
        Debug.DrawLine(transform.position + new Vector3(0f, 0f, fc.maxDistance), transform.position + new Vector3(0f, 0f, fc.maxDistance) + fc.planes[5].normal, Color.green);
        Debug.DrawLine(transform.position + new Vector3(0f, 0f, fc.maxDistance), transform.position + new Vector3(0f, 0f, fc.maxDistance) + fc.planes[5].normal / 2f, Color.blue);
    }
}
