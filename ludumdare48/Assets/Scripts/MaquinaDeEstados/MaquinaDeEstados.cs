﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaquinaDeEstados : MonoBehaviour
{
    public Camera cam;

    public MonoBehaviour patrulla;
    public MonoBehaviour alerta;
    public MonoBehaviour persecucion;
    public MonoBehaviour estadoInicial;

    public MeshRenderer indicadorMesh;

    public int actualMood;

    public enum Mood
    {
        neutral,
        noNeutral,
        agresivo,
        last
    }
    
    public MonoBehaviour estadoActual;

    void Start()
    {
        ActivarEstado(estadoInicial);
       // Debug.Log("pos: " + transform.position.ToString());
    }

    public void ActivarEstado(MonoBehaviour nuevoEstado)
    {
        if (estadoActual!=null)
            estadoActual.enabled = false;
        estadoActual = nuevoEstado;
        estadoActual.enabled = true;
    }

    void Update()
    {
        //Debug.Log("pos: " + transform.position.ToString());
    }
}
