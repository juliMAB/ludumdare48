﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInRoom : MonoBehaviour
{
    const short tam = 6;

    [Header("Game Object")]
    public GameObject go;
    

    [Header("Plane and room")]
    public Plane[] roomPlanes = new Plane[tam];
    public Vector3[] normal = new Vector3[tam];
    public Vector3[] roomVertex = new Vector3[2];

    public static bool InRoom(Vector3 position, Plane[] roomPlanes, int tam)
   {
        for (int i = 0; i < tam; i++)
        {
            for (int j = 0; j < tam - 1; j++)
            {
                if (roomPlanes[i].GetSide(position) != roomPlanes[j].GetSide(position))
                    return false;
            }            
        }

        return true;
   }

    public static bool CheckAllVertexInRoom(GameObject go, Plane[] roomPlanes, int tam)
    {
        Mesh mesh = go.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices; 

        for (int i = 0; i < vertices.Length; i++)
        {
            if (!InRoom(go.transform.position + vertices[i], roomPlanes, tam))
                return false;
        }

        return true;
    }

    public static bool CheckOneVertexInRoom(GameObject go, Plane[] roomPlanes, int tam)
    {
        Mesh mesh = go.GetComponent<MeshFilter>().mesh;
        Vector3[] vertices = mesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            if (InRoom(go.transform.TransformPoint(vertices[i]), roomPlanes, tam))
                return true;
        }

        return false;
    }

    void Start()
    {
        normal[0] = new Vector3(0, 1, 0);// Floor
        normal[1] = new Vector3(0, -1, 0);// Roof
        normal[2] = new Vector3(0, 0, -1); // Front
        normal[3] = new Vector3(0, 0, 1);// Back
        normal[4] = new Vector3(-1, 0, 0); // Right
        normal[5] = new Vector3(1, 0, 0);// Left

        roomVertex[0] = new Vector3(-5, 5, 5); 
        roomVertex[1] = new Vector3(5, -5, -5);

        roomPlanes[0] = new Plane(normal[0], roomVertex[1]);// Floor
        roomPlanes[1] = new Plane(normal[1], roomVertex[0]);// Roof
        roomPlanes[2] = new Plane(normal[2], roomVertex[0]);// Front
        roomPlanes[3] = new Plane(normal[3], roomVertex[1]);// Back
        roomPlanes[4] = new Plane(normal[4], roomVertex[1]);// Right
        roomPlanes[5] = new Plane(normal[5], roomVertex[0]);// Left
    }

    void Update()
    {
        if (CheckAllVertexInRoom(go, roomPlanes, tam))
        {
                print("The object is completely in the room");
        }
        else
        {
                print("The object is not in the room");
        }
        
    }
}
