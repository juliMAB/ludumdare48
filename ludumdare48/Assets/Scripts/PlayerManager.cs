﻿using System;
using System.Collections;
using System.Collections.Generic;
using AK.Wwise;
using Homebrew;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Utility;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class PlayerManager : MonoBehaviour
{

    [Foldout("MANAGER", true)]

    [SerializeField] private CharacterController controller;

    

    private GameObject note=null;

    [Foldout("PLAYER", true)]

    [NonSerialized] public bool somethingOnHead;

    public int vida = 100;

    private bool canGrab;

    public float speed = 12f;

    public float gravity = -9.81f;

    private Vector3 velocity;

    private bool checkCol;

    private float normalHight;

    private float smallHight;

    [SerializeField] private float distance = 10.0f;

    [Foldout("AUDIOMANAGER", true)]

    //private AudioSource m_AudioSource;

    private float speed_sound;

    [SerializeField] public float value;

    [SerializeField] private bool m_UseFovKick;

    [SerializeField] private FOVKick m_FovKick = new FOVKick();

    [SerializeField] [Range(0f, 1f)] private float m_RunstepLenghten;

    [SerializeField] private float m_StepInterval;

    [SerializeField] private bool m_IsWalking;

    [SerializeField] public string RTPC_Nivel_de_Miedo;

    [SerializeField] public int type = 1;

    [SerializeField] private float m_WalkSpeed;

    [SerializeField] private float m_RunSpeed;

   

    //[SerializeField] private AudioClip[] m_FootstepSounds;    // an array of footstep sounds that will be randomly selected from.

    //[SerializeField] private AudioClip m_JumpSound;           // the sound played when character leaves the ground.

    //[SerializeField] private AudioClip m_LandSound;           // the sound played when character touches back on ground.



    [Foldout("FLASHLIGHT", true)]

    [SerializeField] private bool flashequiped;

    [SerializeField] public float charge;

    [SerializeField] private Light myLightlight;

    private float m_StepCycle;

    private float m_NextStep;

    private float normalSpotAngle;

    private float normalIntensity;

    private Vector2 m_Input;

    private float normalLightRange;

    private CharacterController m_CharacterController;

    [Foldout("CAMERA", true)]

    [SerializeField] private Camera cam;

    [SerializeField] private float mouseSensitivity = 100f;

    private Ray ray;

    private float xRotation = 0f;



    private void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
        m_NextStep = m_StepCycle / 2f;
        Cursor.lockState = CursorLockMode.Locked;
        normalHight = controller.height;
        smallHight = controller.height / 2;
        normalSpotAngle = myLightlight.spotAngle;
        normalIntensity = myLightlight.intensity;
        normalLightRange = myLightlight.range;
    }

    void Update()
    {
        UpdateAudio();
        GetInput(out speed_sound);
        ProgressStepCycle(speed_sound);
        UpdatePj();
    }

    private void UpdatePj()
    {
        Click();
        UpdateCamera();
        UpdateRay();
        MovePj();
        FixGravity();
        AplicateGravity();
        UpdateFlashlight();
        Agacharse();
    }
    private void UpdateAudio()
    {
        UpdateRtpcValue();
    }
    public void UpdateRtpcValue()
    {
        AkSoundEngine.SetRTPCValue("RTPC_Nivel_de_Miedo", value);
    }

    public bool PickUpNote()
    {
        CheckNote();
        if (!canGrab) return false;
        if (!Input.GetKeyDown(KeyCode.E)) return false;
        Destroy(note);
        return true;
    }
    private void CheckNote()
    {
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distance))
        {
            if (hit.transform.GetComponent<Note>())
            {
                Debug.Log("Puedoagarrarelobjeto");
                note = hit.transform.gameObject;
                canGrab = true;
            }
            else
            {
                canGrab = false;
            }
        }
    }
    void Agacharse()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            controller.height = smallHight;
        }
        else
        {
            if (!somethingOnHead)
            {
                controller.height = normalHight;
            }
        }
        
    }


    private void ProgressStepCycle(float speed)
    {
        if (m_CharacterController.velocity.sqrMagnitude > 0 && (m_Input.x != 0 || m_Input.y != 0))
        {
            m_StepCycle += (m_CharacterController.velocity.magnitude + (speed * (m_IsWalking ? 1f : m_RunstepLenghten))) *
                           Time.fixedDeltaTime;
        }

        if (!(m_StepCycle > m_NextStep))
        {
            return;
        }

        m_NextStep = m_StepCycle + m_StepInterval;

        PlayFootStepAudio();
    }


    private void PlayFootStepAudio()
    {
        if (!m_CharacterController.isGrounded)
        {
            return;
        }

        AkSoundEngine.PostEvent("Caminar", gameObject);
    }

    private void PlayLinternAudio()
    {
        AkSoundEngine.PostEvent("Play_Linterna", gameObject);
    }


    private void GetInput(out float speed)
    {
        // Read input
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        bool waswalking = m_IsWalking;

#if !MOBILE_INPUT
        // On standalone builds, walk/run speed is modified by a key press.
        // keep track of whether or not the character is walking or running
        m_IsWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
        // set the desired speed to be walking or running
        speed = m_IsWalking ? m_WalkSpeed : m_RunSpeed;
        m_Input = new Vector2(horizontal, vertical);

        // normalize input if it exceeds 1 in combined length:
        if (m_Input.sqrMagnitude > 1)
        {
            m_Input.Normalize();
        }

        // handle speed change to give an fov kick
        // only if the player is going to a run, is running and the fovkick is to be used
        if (m_IsWalking != waswalking && m_UseFovKick && m_CharacterController.velocity.sqrMagnitude > 0)
        {
            StopAllCoroutines();
            StartCoroutine(!m_IsWalking ? m_FovKick.FOVKickUp() : m_FovKick.FOVKickDown());
        }
    }

    void UpdateFlashlight()
    {
        myLightlight.enabled = flashequiped;
        if (charge>0)
        {
            if (flashequiped)
            {
                charge -= Time.deltaTime;
                value += Time.deltaTime;
                if (Input.GetMouseButtonDown(1))
                {
                    myLightlight.spotAngle = normalSpotAngle / 2;
                    myLightlight.intensity = normalIntensity * 5;
                    myLightlight.range = normalLightRange * 3;
                    charge -= Time.deltaTime;
                }

                if (Input.GetMouseButtonUp(1))
                {
                    myLightlight.spotAngle = normalSpotAngle;
                    myLightlight.intensity = normalIntensity;
                    myLightlight.range = normalLightRange;
                }
            }
        }
        else
        {
            flashequiped = false;
        }
        
    }
    void FixGravity()
    {
        if (controller.isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }
    }
    void AplicateGravity()
    {
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
    }
    void UpdateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        cam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }
    void UpdateRay()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 50, Color.yellow);
    }
    private void Click()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        PlayLinternAudio();
        if (charge>0)
        {
            flashequiped = !flashequiped;
        }


        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, distance)) return;
        if (hit.transform.CompareTag("Untagged"))
        {
        }

        if (hit.transform.parent != null)
        {
        }
    }

    void MovePj()
    {
        float x = Input.GetAxis($"Horizontal");
        float z = Input.GetAxis($"Vertical");
        var transform1 = transform;
        Vector3 move = transform1.right * x + transform1.forward * z;
        controller.Move(move * speed * Time.deltaTime);
    }
    
    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (!checkCol)
        {
            if (hit.transform.parent != null)
            {
                checkCol = true;
            }
        }
        if (hit.transform.CompareTag("Untagged"))
        {
            checkCol = false;
        }
        if (hit.transform.CompareTag("Fire"))
        {
            vida--;
        }
    }
}