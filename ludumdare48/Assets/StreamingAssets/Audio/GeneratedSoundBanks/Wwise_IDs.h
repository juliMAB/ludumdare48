/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID AGARRAR_NOTA = 3887413290U;
        static const AkUniqueID APARECE_BICHO = 1679374644U;
        static const AkUniqueID CAMINAR = 621083434U;
        static const AkUniqueID DOOR_CLOSE = 128625444U;
        static const AkUniqueID DOOR_OPEN = 535830432U;
        static const AkUniqueID MENU_OFF = 3036877654U;
        static const AkUniqueID MENU_ON = 188046872U;
        static const AkUniqueID MUERTE_FINAL = 2254227550U;
        static const AkUniqueID PAUSE_OFF = 3967028551U;
        static const AkUniqueID PAUSE_ON = 3537680115U;
        static const AkUniqueID PLAY_AMBIENT = 1562304622U;
        static const AkUniqueID PLAY_LINTERNA = 4187561871U;
        static const AkUniqueID PLAY_PASOS_BICHO = 1665861936U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace ESTADO
        {
            static const AkUniqueID GROUP = 868550779U;

            namespace STATE
            {
                static const AkUniqueID INGAME = 984691642U;
                static const AkUniqueID MENU = 2607556080U;
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID PAUSE = 3092587493U;
            } // namespace STATE
        } // namespace ESTADO

        namespace INGAME
        {
            static const AkUniqueID GROUP = 984691642U;

            namespace STATE
            {
                static const AkUniqueID ALIVE = 655265632U;
                static const AkUniqueID DEAD = 2044049779U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace INGAME

        namespace PAUSE
        {
            static const AkUniqueID GROUP = 3092587493U;

            namespace STATE
            {
                static const AkUniqueID NONE = 748895195U;
                static const AkUniqueID OFF = 930712164U;
                static const AkUniqueID ON = 1651971902U;
            } // namespace STATE
        } // namespace PAUSE

    } // namespace STATES

    namespace GAME_PARAMETERS
    {
        static const AkUniqueID RTPC_NIVEL_DE_MIEDO = 3731709508U;
    } // namespace GAME_PARAMETERS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID NEW_SOUNDBANK = 4072029455U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
