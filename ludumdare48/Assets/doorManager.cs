﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorManager : MonoBehaviour
{
    private bool open=false;
    private bool dentro = false;
    private GameObject pivot;
    private readonly Vector3 goTo=Vector3.zero;
    private bool entro = false;
    [SerializeField] public bool loked=true;

    void Start()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (loked)return;
            if (Input.GetKeyDown(KeyCode.E))
            {
                AkSoundEngine.PostEvent("Door_Open", gameObject);
                open = !open;

                dentro = true;
            }

            if (open)
            {
                Abrir();
            }
            else
            {
                Cerrar();
            }
        }
    }
    void Abrir()
    {
        transform.GetChild(0).localEulerAngles = new Vector3(goTo.x,goTo.y + 90,goTo.z);
    }
    void Cerrar()
    {
        transform.GetChild(0).localEulerAngles = new Vector3(goTo.x, goTo.y, goTo.z);
    }

    public bool Dentro()
    {
        return dentro;
    }
}

