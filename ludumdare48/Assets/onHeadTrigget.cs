﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onHeadTrigget : MonoBehaviour
{
    public PlayerManager pj;



    private void OnTriggerEnter(Collider other)
    {
        pj.somethingOnHead = true;
    }

    private void OnTriggerExit(Collider other)
    {
        pj.somethingOnHead = false;
    }
}
