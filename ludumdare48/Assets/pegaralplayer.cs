﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pegaralplayer : MonoBehaviour
{
    public PlayerManager pj;
    public bool LoTieneAdentro;
    public float timer = 3;

    private void OnTriggerStay(Collider other)
    {

        if (other.gameObject.GetComponent<PlayerManager>())
        {
            timer--;
            if (timer <= 0)
            {
                pj.vida -= 20;
                timer = 3;
            }
            LoTieneAdentro = true;
        }
    }
}
