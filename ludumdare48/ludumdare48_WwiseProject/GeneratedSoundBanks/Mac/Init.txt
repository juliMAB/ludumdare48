State Group	ID	Name			Wwise Object Path	Notes
	868550779	Estado			\Default Work Unit\Estado	
	984691642	Ingame			\Default Work Unit\Ingame	
	3092587493	Pause			\Default Work Unit\Pause	

State	ID	Name	State Group			Notes
	748895195	None	Estado			
	984691642	Ingame	Estado			
	2607556080	Menu	Estado			
	3092587493	Pause	Estado			
	655265632	Alive	Ingame			
	748895195	None	Ingame			
	2044049779	Dead	Ingame			
	748895195	None	Pause			
	930712164	Off	Pause			
	1651971902	On	Pause			

Audio Bus	ID	Name			Wwise Object Path	Notes
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

