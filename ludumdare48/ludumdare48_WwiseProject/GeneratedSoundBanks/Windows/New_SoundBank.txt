Event	ID	Name			Wwise Object Path	Notes
	128625444	Door_Close			\Default Work Unit\Door_Close	
	188046872	Menu_On			\Default Work Unit\Menu_On	
	535830432	Door_Open			\Default Work Unit\Door_Open	
	621083434	Caminar			\Default Work Unit\Caminar	
	1562304622	Play_Ambient			\Default Work Unit\Play_Ambient	
	1665861936	Play_Pasos_Bicho			\Default Work Unit\Play_Pasos_Bicho	
	1679374644	Aparece_Bicho			\Default Work Unit\Aparece_Bicho	
	2254227550	Muerte_Final			\Default Work Unit\Muerte_Final	
	3036877654	Menu_Off			\Default Work Unit\Menu_Off	
	3537680115	Pause_On			\Default Work Unit\Pause_On	
	3887413290	Agarrar_Nota			\Default Work Unit\Agarrar_Nota	
	3967028551	Pause_Off			\Default Work Unit\Pause_Off	
	4187561871	Play_Linterna			\Default Work Unit\Play_Linterna	

State Group	ID	Name			Wwise Object Path	Notes
	868550779	Estado			\Default Work Unit\Estado	
	984691642	Ingame			\Default Work Unit\Ingame	
	3092587493	Pause			\Default Work Unit\Pause	

State	ID	Name	State Group			Notes
	748895195	None	Estado			
	984691642	Ingame	Estado			
	2607556080	Menu	Estado			
	3092587493	Pause	Estado			
	655265632	Alive	Ingame			
	748895195	None	Ingame			
	2044049779	Dead	Ingame			
	748895195	None	Pause			
	930712164	Off	Pause			
	1651971902	On	Pause			

Custom State	ID	Name	State Group	Owner		Notes
	81983159	Pause	Estado	\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient		
	145070132	On	Pause	\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient		
	208666442	Menu	Estado	\Actor-Mixer Hierarchy\Default Work Unit\Gemidos horribles		
	239571959	Menu	Estado	\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient		
	360501958	On	Pause	\Actor-Mixer Hierarchy\Default Work Unit\Gemidos horribles		
	398645287	Dead	Ingame	\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient		
	852556108	Ingame	Estado	\Interactive Music Hierarchy\Default Work Unit\Menu Ambient		
	985011112	Menu	Estado	\Actor-Mixer Hierarchy\Default Work Unit\HeartBeat		
	1000244883	Pause	Estado	\Interactive Music Hierarchy\Default Work Unit\Menu Ambient		

Game Parameter	ID	Name			Wwise Object Path	Notes
	3731709508	RTPC_Nivel_de_Miedo			\Default Work Unit\RTPC_Nivel_de_Miedo	

Effect plug-ins	ID	Name	Type				Notes
	1588715066	Echoes_Linear	Wwise Delay			
	3043085106	High_Impact	Wwise Compressor			
	3546444380	Big_Garage1	Wwise Matrix Reverb			

Source plug-ins	ID	Name	Type		Wwise Object Path	Notes
	6032813	Wwise Silence	Wwise Silence		\Actor-Mixer Hierarchy\Default Work Unit\Gemidos horribles\Silencio\Wwise Silence	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	10543680	Ruido con la boca feo 2	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Ruido con la boca feo 2_8581B456.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ruido boca feo\Ruido con la boca feo 2		1110772
	53155271	cadena rvb	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\cadena rvb_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient\Ambient\cadena rvb		1812988
	77424094	Brass 1	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Brass 1_0ADA8E2D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Rugido_Muerte\Brass 1		556988
	99845447	Ruido con la boca feo	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Ruido con la boca feo _6CE30B60.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ruido boca feo\Ruido con la boca feo		1408844
	148257290	Cerrar puerta	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Puerta2_30EEF393.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Cerrar\Cerrar puerta		188920
	174030191	Pasos_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_C7E0322C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_01		75924
	190865448	Pasos	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_849F0D0B.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos		71284
	201562512	Ambient	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Ambient_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient\Ambient\Ambient		6028224
	227092454	Brass 3	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Brass 3_C5C4CEC1.wem		\Actor-Mixer Hierarchy\Default Work Unit\Rugido_Muerte\Brass 3		948840
	268879591	Rugido_Muerte_02	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Elefante2_0AA0DD3F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Rugido_Muerte\Rugido_Muerte_02		414684
	277741577	Linterna_06	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_E53021AD.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_06		47420
	283836067	Abrir puerta_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Puerta2_AC367BC7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Abrir\Abrir puerta_01		230884
	318989494	Pisadas_Bicho	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pisadas_Bicho_39992AA7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos_Bicho\Pisadas_Bicho		223240
	328319299	Menu Ambient	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Menu Ambient_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\Menu Ambient\Menu Ambient\Menu Ambient		14154156
	344029034	Desesperacion	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Proyecto Te quiero mucho_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient\Ambient\Desesperacion		13406544
	345663660	Linterna_05	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_2B920C36.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_05		64888
	350687137	Pasos_04	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_1D77AC0F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_04		107820
	356820753	Cerrar puerta_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Puerta2_0EFEC4EA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Cerrar\Cerrar puerta_01		200908
	360438584	Pasos_02	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_6BC7AE56.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_02		108628
	406225242	Triangulo	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Triangulo_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Triangulo		1781824
	454498167	cajita musical	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\cajita musical_D79FB2B7.wem		\Interactive Music Hierarchy\Default Work Unit\Ingame_Ambient\Ambient\cajita musical		5766080
	455334971	Pasos_06	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_3B2D480A.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_06		89116
	456228001	Pisadas_Bicho_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pisadas_Bicho_984CBFCC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos_Bicho\Pisadas_Bicho_01		232736
	457492234	Rugido_Muerte	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Elefante0_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Rugido_Muerte\Rugido_Muerte		45514
	512725960	Abrir puerta	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Puerta1_9638DA5F.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Abrir\Abrir puerta		226732
	520431937	Cerrar puerta_02	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Puerta1_403DB3EC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Cerrar\Cerrar puerta_02		206052
	546794592	Gemidos horribles 2	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Gemidos horribles 2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Gemidos horribles\Gemidos horribles\Gemidos horribles 2		2882496
	548986511	Agarrar papel_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Agarrar papel_B137DBED.wem		\Actor-Mixer Hierarchy\Default Work Unit\Agarrar nota\Agarrar papel_01		179172
	556688402	Pasos_09	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_2D889FA9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_09		85984
	596529581	Linterna	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_485B3B3E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna		58528
	629236925	Linterna_07	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_993CEF46.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_07		52032
	675526251	Linterna_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_E2DFB4DC.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_01		50696
	740960393	Pasos_05	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_F18C9980.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_05		97524
	756056145	Agarrar papel_02	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Agarrar papel_78241B8B.wem		\Actor-Mixer Hierarchy\Default Work Unit\Agarrar nota\Agarrar papel_02		134768
	801256669	Gemidos horribles 3	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Gemidos horribles 3_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Gemidos horribles\Gemidos horribles\Gemidos horribles 3		2882496
	822845981	Linterna_03	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_8366A271.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_03		54736
	837342747	Pisadas_Bicho_03	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pisadas_Bicho_4E661AE6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos_Bicho\Pisadas_Bicho_03		140720
	837660142	Cerrar puerta_03	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Puerta1_4DFC1E27.wem		\Actor-Mixer Hierarchy\Default Work Unit\Puerta Cerrar\Cerrar puerta_03		168600
	850046991	HeartBeat	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\HeartBeat_D79FB2B7.wem		\Actor-Mixer Hierarchy\Default Work Unit\HeartBeat\HeartBeat\HeartBeat		356928
	909890485	Pasos_08	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_65A18FC7.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_08		79536
	910258430	Linterna_04	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_6149D78D.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_04		50296
	926144316	Pasos_03	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_388A04D0.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_03		112520
	951699457	Linterna_02	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Linterna_BC727E8E.wem		\Actor-Mixer Hierarchy\Default Work Unit\Linterna\Linterna_02		54736
	967617589	Pisadas_Bicho_02	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pisadas_Bicho_43E93701.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos_Bicho\Pisadas_Bicho_02		208996
	969649722	Agarrar papel	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Agarrar papel_F050490A.wem		\Actor-Mixer Hierarchy\Default Work Unit\Agarrar nota\Agarrar papel		198544
	984347666	Brass 2	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Brass 2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Rugido_Muerte\Brass 2		716924
	1002973573	Rugido_Muerte_01	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Elefante1_710543AA.wem		\Actor-Mixer Hierarchy\Default Work Unit\Rugido_Muerte\Rugido_Muerte_01		352864
	1018220968	Gemidos horribles 1	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Gemidos horribles 1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\Gemidos horribles\Gemidos horribles\Gemidos horribles 1		2358208
	1048075170	Agarrar papel_03	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Agarrar papel_11A24EE6.wem		\Actor-Mixer Hierarchy\Default Work Unit\Agarrar nota\Agarrar papel_03		134768
	1053697654	Pasos_07	C:\Users\lucas\Documents\GitHub\ludumdare48\ludumdare48\ludumdare48_WwiseProject\.cache\Windows\SFX\Pasos_DF31035C.wem		\Actor-Mixer Hierarchy\Default Work Unit\Pasos\Pasos_07		88728

